FROM python:3.10

# Update the repository
RUN apt-get update && apt-get upgrade -y

# Install the python packages
RUN python -m pip install python-telegram-bot \
    && python -m pip install requests

RUN mkdir /opt/bot

# Link shared volume ./data to /data
RUN mkdir /data \
    && ln -s /data /opt/bot/data

COPY *.py /opt/bot/

WORKDIR /opt/bot

ENTRYPOINT ["python3", "caissecafebot.py", "user.py", "cafe.py", "utils.py"]
