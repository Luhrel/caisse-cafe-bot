from DBHelper import DB


class Cafe:
    def __init__(self, brand):
        self.brand_id = brand[0]
        self.group_id = brand[1]
        self.name = brand[2]
        self.stock = brand[3]

    def getID(self):
        return self.brand_id

    def getGroupID(self):
        return self.group_id

    def getName(self):
        return self.name

    def getStock(self):
        return self.stock

    @staticmethod
    def fromID(brand_id):
        return Cafe(DB.getBrandFromID(brand_id))

    @staticmethod
    def fromName(group_id, name):
        cur = DB.getBrandFromName(group_id, name)
        if len(cur):
            return Cafe(cur[0])
        return None

    @staticmethod
    def getBrands(group_id):
        return [Cafe(x) for x in DB.getAllBrands(group_id)]

    @staticmethod
    def getStocks(group_id):
        return DB.getStock(group_id)

    @staticmethod
    def getTopCafe(group_id, date_time):
        return DB.getTopBrands(group_id, date_time)

    @staticmethod
    def getTopDrinker(group_id, date):
        return DB.getTopDrinker(group_id, date)

    @staticmethod
    def getBalance(group_id):
        balance = DB.getBalance(group_id)
        dico = {}
        for userid, nbcafe in balance:
            dico[userid] = nbcafe
        return dico

    @staticmethod
    def getUserBalance(group_id, user_id):
        return Cafe.toDict(DB.getUserBalance(user_id, group_id))

    @staticmethod
    def getPrice(group_id):
        price = DB.getPrice(group_id)
        if price is None:
            DB.genPrice(group_id)
            price = DB.getPrice(group_id)
        return price[0]

    @staticmethod
    def setPrice(group_id, price):
        return DB.setPrice(group_id, price)

    @staticmethod
    def getUserStats(group_id, user_id):
        return DB.getUserStats(group_id, user_id)

    @staticmethod
    def payoff(group_id, user_id):
        return DB.payoff(user_id, group_id)

    @staticmethod
    def addBrand(group_id, name):
        cafe = Cafe.fromName(group_id, name)
        if cafe is None:
            return DB.addBrand(group_id, name)
        DB.setDeletedBrand(group_id, cafe.getID(), 0)
        return cafe

    @staticmethod
    def removeBrand(group_id, brand_id):
        return DB.setDeletedBrand(group_id, brand_id, 1)

    @staticmethod
    def addStock(group_id, brand_id, quantity):
        return DB.incStock(brand_id, group_id, quantity)

    @staticmethod
    def drinkCafe(group_id, user_id, brand_id):
        cafe = DB.addCafe(user_id, group_id, brand_id)
        Cafe.addStock(group_id, brand_id, -1)
        return cafe

    @staticmethod
    def toDict(array_2d):
        array_dict = []
        for array in array_2d:
            dico = {}
            for key, val in zip(["user", "value"], array):
                dico[key] = val
            array_dict.append(dict)
