from DBHelper import DB
from utils import Utils
import logging

logger = logging.getLogger("caissecafebot")


class User:
    def __init__(self, user):
        self.user_id = user[0]
        self.username = user[1]
        self.first_name = user[2]
        self.last_name = user[3]

    def getID(self):
        return self.user_id

    def getUsername(self):
        return self.username

    def getFirstName(self):
        return self.first_name

    def getLastName(self):
        return self.last_name

    @staticmethod
    def update(user_id, username, first_name, last_name):
        DB.updateUser(user_id, username, first_name, last_name)

    @staticmethod
    def add(user_id, username, first_name, last_name):
        DB.addUser(user_id, username, first_name, last_name)

    @staticmethod
    def createOrUpdate(user_id, chat_id, bot):
        member = Utils.getChatMemberFromID(user_id, chat_id, bot)
        if member is None:
            return

        user = DB.getUser(user_id)
        logger.debug(f"user: {user}, member : {member}")

        username = member.user.username
        first_name = member.user.first_name
        last_name = member.user.last_name

        if user is None:
            logger.debug(f"Adding a new user !")
            User.add(user_id, username, first_name, last_name)
        else:
            logger.debug(f"Updating user !")
            User.update(user_id, username, first_name, last_name)

    # Updates the user if chat_id is not None
    @staticmethod
    def fromID(user_id):
        user = DB.getUser(user_id)
        if user is None:
            return None
        return User(user)
