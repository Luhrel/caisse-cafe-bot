# -*- coding: utf-8 -*-

from data.config import config as config
from cafe import Cafe
import logging


from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
import telegram.constants as constants
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler
from telegram.utils.helpers import escape_markdown

from user import User
from utils import Utils

logging.basicConfig(format='%(asctime)s - %(levelname)s (%(funcName)s()) - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger("caissecafebot")

CURRENCY = b'\xF0\x9F\x92\xB5'.decode()  # $ utf-32 sign


##################################
class Bot:
    bot = None

    ### Command coffee ###

    # /cafe : commander un cafe
    @staticmethod
    def cafe(update, context):
        # Do nothing if the command is from an edited message
        if update.edited_message is not None:
            return
        brands = Cafe.getBrands(update.message.chat_id)

        if len(brands) == 0:
            update.message.reply_text(
                "Aucune sorte de café n'est définie.\n"
                + "Tapez /addbrand <name> pour en ajouter une."
            )
            return

        # TODO: Récupérer le stock total d'une manière plus efficace
        total_stock = 0
        keyboard = []
        for cafe in brands:
            if cafe.getStock() == 0:  # n'affiche pas les cafés sans stock
                continue
            total_stock += cafe.getStock()
            line = [InlineKeyboardButton(
                "{} ({} ☕)".format(cafe.getName(), cafe.getStock()),
                callback_data="#cafe&{}&{}".format(cafe.getID(), update.message.chat_id)
            )]
            keyboard.append(line)

        if total_stock == 0:
            update.message.reply_text(
                "Aucun café n'est disponible !\n"
                + "Tapez /addstock pour en ajouter."
            )
            return

        reply_markup = InlineKeyboardMarkup(keyboard)
        update.message.reply_text('Choisissez quelle sorte de café vous voulez :', reply_markup=reply_markup)

    ### Get informations ###

    # /balance : afficher l'état des comptes
    @staticmethod
    def balance(update, context):
        # Do nothing if the command is from an edited message
        if update.edited_message is not None:
            return
        dico = Cafe.getBalance(update.message.chat_id)
        logger.debug(f"dico : {dico}")
        if len(dico) == 0:
            update.message.reply_text("Vous n'avez commandé aucun café.")
            return

        text = "__État des comptes :__"
        for user_id in dico:
            user = User.fromID(user_id)
            logger.debug(f"user : {user}")
            nb_cafe = int(dico[user_id])
            logger.debug(f"nb_cafe : {nb_cafe}")
            price = Utils.formatPrice(nb_cafe * Cafe.getPrice(update.message.chat_id))
            logger.debug(f"price : {price}")
            pl = "s" if nb_cafe > 1 else ""
            username = Utils.getPrettyUsername(user)
            logger.debug(f"username : {username}")
            text += escape_markdown(f"\n{username} doit {nb_cafe} café{pl} ({price} {CURRENCY})", 2)
        logger.debug(f"text : {text}")
        update.message.reply_text(text, parse_mode=ParseMode.MARKDOWN_V2)

    # /stocks : afficher l'état des stocks de café
    @staticmethod
    def stock(update, context):
        # Do nothing if the command is from an edited message
        if update.edited_message is not None:
            return
        brands = Cafe.getBrands(update.message.chat_id)
        logger.debug(f"brands : {brands}")

        no_stock_msg = "Il n'y a aucun café en stock !"
        if len(brands) == 0:
            update.message.reply_text(no_stock_msg)
            return

        text = "__Liste des cafés disponibles :__\n"
        tmp = []
        for cafe in brands:
            nb = cafe.getStock()
            if nb:
                tmp.append((cafe.getName(), nb))

        if len(tmp) > 0:
            for cafe in tmp:
                text += escape_markdown(f"{cafe[0]} ({cafe[1]} ☕)\n", 2)
        else:
            # Aucun café en stock
            update.message.reply_text(no_stock_msg)
            return
        logger.debug(f"text : {text}")
        update.message.reply_text(text, parse_mode=ParseMode.MARKDOWN_V2)

    # /topdrinker [week|month|all] : afficher les plus grand buveurs
    @staticmethod
    def topdrinker(update, context):
        # Do nothing if the command is from an edited message
        if update.edited_message is not None:
            return
        # TODO: impl [week|month|all]
        date, date_info = Utils.getDate(context.args)
        """
        if date is None:
            update.message.reply_text("Utilisation: /topdrinker [week|month|year]")
            return
        """
        text = "__Plus gros buveurs de cafés du mois "
        # text += date_info + " "
        text += ":__"

        stats = Cafe.getTopDrinker(update.message.chat_id, date)
        logger.debug(f"stats : {stats}")
        if len(stats) == 0:
            update.message.reply_text("Aucun café n'a été consommé ce mois-ci !")
            return

        for user_id, nb_cafes in stats:
            user = User.fromID(user_id)
            logger.debug(f"user : {user}")
            username = Utils.getPrettyUsername(user)
            logger.debug(f"username : {username}")
            text += escape_markdown(f"\n{username} ({nb_cafes} ☕)", 2)
        logger.debug(f"text : {text}")
        update.message.reply_text(text, parse_mode=ParseMode.MARKDOWN_V2)

    # /topcafe [week|month|all] : affiche les meilleurs sortes de cafés
    @staticmethod
    def topcafe(update, context):
        # Do nothing if the command is from an edited message
        if update.edited_message is not None:
            return
        # TODO: impl [week|month|all]
        date, date_info = Utils.getDate(context.args)
        """
        if date is None:
            update.message.reply_text("Utilisation: /topcafe [week|month|year]")
            return
        """
        text = "__Cafés les plus appréciés "
        # text += date_info + " "
        text += ":__"

        stats = Cafe.getTopCafe(update.message.chat_id, date)
        logger.debug(f"stats : {stats}")
        if len(stats) == 0:
            update.message.reply_text("Aucun café n'a été consommé ce mois-ci !")
            return

        for brand_name, nb_cafes in stats:
            text += f"\n_{escape_markdown(brand_name, 2)}_ \\({nb_cafes} ☕\\)"
        logger.debug(f"text : {text}")
        update.message.reply_text(text, parse_mode=ParseMode.MARKDOWN_V2)

    ### Admin methods ###

    # /payoff : enlever les dettes d'un utilisateur
    @staticmethod
    def payoff(update, context):
        # Do nothing if the command is from an edited message
        if update.edited_message is not None:
            return
        if not Utils.isCreator(update.message.from_user.id, update.message.chat_id, Bot.bot):
            Bot.printErrNotAdmin(update)
            return

        dico = Cafe.getBalance(update.message.chat_id)
        logger.debug("payoff() dico : {}".format(dico))
        if len(dico) == 0:
            update.message.reply_text("Aucun utilisateur n'a de café ☕ à rembourser !")
            return
        keyboard = []
        for user_id in dico:
            user = User.fromID(user_id)
            username = Utils.getPrettyUsername(user)
            price = Cafe.getPrice(update.message.chat_id)
            price = Utils.formatPrice(price * dico[user_id])
            line = [InlineKeyboardButton(
                f"{username} ({dico[user_id]} ☕ = {price} {CURRENCY})",
                callback_data="#payoff&{}&{}".format(user_id, update.message.chat_id)
            )]
            keyboard.append(line)
        reply_markup = InlineKeyboardMarkup(keyboard)
        update.message.reply_text("Quel utilisateur a remboursé ses cafés ☕ ?", reply_markup=reply_markup)

    # /addstock [quantity] : redéfinir l'état des stocks
    @staticmethod
    def addStock(update, context):
        # Do nothing if the command is from an edited message
        if update.edited_message is not None:
            return
        # TODO: ou si admin ?
        if not Utils.isCreator(update.message.from_user.id, update.message.chat_id, Bot.bot):
            Bot.printErrNotAdmin(update)
            return

        brands = Cafe.getBrands(update.message.chat_id)
        if len(brands) == 0:
            update.message.reply_text(
                "Aucune sorte de café n'est définie.\n"
                + "Tapez /addbrand <nom de la sorte de café> pour en ajouter."
            )
            return

        args = context.args
        nb_to_add = 0
        if len(args) == 1:
            try:
                nb_to_add = int(args[0])
            except ValueError:
                update.message.reply_text(f'"{args[0]}" n\'est pas un nombre !')
                return
        elif len(args) > 1:
            update.message.reply_text("Utilisation: /addstock [quantity]")
            return
        logger.debug(f"nb_to_add: {nb_to_add}")

        keyboard = []
        for cafe in brands:
            line = [InlineKeyboardButton(
                "{} ({} ☕)".format(cafe.getName(), cafe.getStock()),
                callback_data="#addStock&{}&{}&{}".format(cafe.getID(), nb_to_add, update.message.chat_id)
            )]
            keyboard.append(line)

        reply_markup = InlineKeyboardMarkup(keyboard)
        logger.debug("addStock() keyboard : {}".format(reply_markup.to_json()))
        update.message.reply_text('Choisissez une sorte de café :', reply_markup=reply_markup)

    """
    @staticmethod
    def showBrandList(update):
        s = "Liste des sortes de café :\n\n"
        for cafe in Cafe.getBrands(update.message.chat_id):
            s += "#{} | {} ({})\n".format(cafe.getID(), cafe.getName(), cafe.getStock())
        update.message.reply_text(s)
    """

    # /addbrand {name} : ajouter une sorte de cafe
    @staticmethod
    def addCoffeeBrand(update, context):
        # Do nothing if the command is from an edited message
        if update.edited_message is not None:
            return
        if not Utils.isCreator(update.message.from_user.id, update.message.chat_id, Bot.bot):
            Bot.printErrNotAdmin(update)
            return

        new_brand = " ".join(context.args)
        if new_brand == "":
            update.message.reply_text("Utilisation: /addbrand <nom de la sorte de café>")
            return
        # Limite le nom de la sorte de café à 50 caractères
        if len(new_brand) > 50:
            update.message.reply_text("Le nom de la sorte de café est trop long (max 50 caractères).")
            return

        Cafe.addBrand(update.message.chat_id, new_brand)
        new_brand = escape_markdown(new_brand, 2)
        logger.debug(f"new_brand: {new_brand}")
        update.message.reply_text(f"La sorte de café _{new_brand}_ a été ajoutée \\!",
                                  parse_mode=ParseMode.MARKDOWN_V2)

    # /removebrand : supprimer une sorte de café
    @staticmethod
    def removeCoffeeBrand(update, context):
        # Do nothing if the command is from an edited message
        if update.edited_message is not None:
            return
        if not Utils.isCreator(update.message.from_user.id, update.message.chat_id, Bot.bot):
            Bot.printErrNotAdmin(update)
            return

        brands = Cafe.getBrands(update.message.chat_id)
        if len(brands) == 0:
            update.message.reply_text("Aucune sorte de café n'est définie.")
            return

        keyboard = []
        for cafe in brands:
            line = [InlineKeyboardButton(
                "{} ({})".format(cafe.getName(), cafe.getStock()),
                callback_data="#removeBrand&{}&{}".format(cafe.getID(), update.message.chat_id)
            )]
            keyboard.append(line)
        reply_markup = InlineKeyboardMarkup(keyboard)
        update.message.reply_text('Choisissez la sorte de café que vous voulez supprimer :',
                                  reply_markup=reply_markup)

    @staticmethod
    def setPrice(update, context):
        # Do nothing if the command is from an edited message
        if update.edited_message is not None:
            return
        if not Utils.isCreator(update.message.from_user.id, update.message.chat_id, Bot.bot):
            Bot.printErrNotAdmin(update)
            return

        args = context.args
        if len(args) == 0:
            update.message.reply_text("Utilisation: /setprice <prix>")
            return

        try:
            new_price = float(args[0])
        except ValueError:
            update.message.reply_text("Le prix doit être un nombre.")
            return
        if new_price < 0:
            update.message.reply_text("Le prix ne peut pas être négatif.")
            return

        Cafe.setPrice(update.message.chat_id, new_price)
        price = escape_markdown(Utils.formatPrice(new_price), 2)
        logger.debug(f"new_price: {new_price}, price: {price}")
        update.message.reply_text(f"Le prix d'un café a été défini à {price} {CURRENCY} \\!",
                                  parse_mode=ParseMode.MARKDOWN_V2)

    # Handlers

    @staticmethod
    def callbackHandler(update, context):
        """Parses the CallbackQuery and updates the message text."""
        query = update.callback_query
        query.answer()
        data = query.data
        logger.debug(f"query: {query}")

        if data.startswith("#addStock"):
            if not Utils.isCreator(query.from_user.id, query.message.chat_id, Bot.bot):
                return

            cb_id, coffee_id, quantity, chat_id = query.data.split("&")
            quantity = int(quantity)
            logger.debug(f"cb_id: {cb_id}, coffee_id: {coffee_id}, quantity: {quantity}, chat_id: {chat_id}")
            if quantity + int(Cafe.fromID(coffee_id).getStock()) < 0:
                quantity = 0

            if quantity == 0:
                keyboard = []
                line = []
                for i, nb in enumerate(range(10, 70, 10)):
                    button = InlineKeyboardButton(
                        "{}".format(nb),
                        callback_data="#addStock&{}&{}&{}".format(coffee_id, nb, chat_id))
                    line.append(button)
                    if i % 3 == 2:
                        keyboard.append(line)
                        line = []
                # if line == []
                if not line:
                    keyboard.append(line)

                reply_markup = InlineKeyboardMarkup(keyboard)
                logger.debug("reply_markup: {}".format(reply_markup.to_json()))
                query.edit_message_text('Choisissez une quantité à ajouter :', reply_markup=reply_markup)
            else:
                Cafe.addStock(chat_id, coffee_id, quantity)
                cafe = Cafe.fromID(coffee_id)
                name = cafe.getName()
                logger.debug(f"stock added: {name} ({quantity})")
                pl = "s" if quantity > 1 or quantity < -1 else ""
                name = escape_markdown(name, 2)
                quantity = escape_markdown(str(quantity), 2)
                logger.debug(f"name: {name}, quantity: {quantity}")
                query.edit_message_text(f"{quantity} cafés _{name}_ ☕ ont été ajouté{pl} au stock \\!",
                                        parse_mode=ParseMode.MARKDOWN_V2)

        elif data.startswith("#cafe"):
            cb_id, brand_id, chat_id = query.data.split("&")
            user_id = update.callback_query.from_user.id
            logger.debug(f"user_id: {user_id}")
            brand = Cafe.fromID(brand_id).getName()
            logger.debug(f"brand: {brand}")

            User.createOrUpdate(user_id, chat_id, Bot.bot)
            user = User.fromID(user_id)
            username = Utils.getPrettyUsername(user)
            logger.debug(f"username: {username}")

            Cafe.drinkCafe(chat_id, user_id, brand_id)
            query.edit_message_text(text=f"☕ {username} a commandé un café {brand} ☕")
        elif data.startswith("#payoff"):
            if not Utils.isCreator(query.from_user.id, query.message.chat_id, Bot.bot):
                return

            cb_id, user_id, chat_id = query.data.split("&")
            Cafe.payoff(chat_id, user_id)
            user = User.fromID(user_id)
            username = Utils.getPrettyUsername(user)
            logger.debug(f"username: {username}")
            query.edit_message_text(f"{username} a remboursé ses cafés ☕ !")
        elif data.startswith("#removeBrand"):
            if not Utils.isCreator(query.from_user.id, query.message.chat_id, Bot.bot):
                return

            cb_id, brand_id, chat_id = query.data.split("&")
            cafe = Cafe.fromID(brand_id)
            name = escape_markdown(cafe.getName(), 2)

            if cafe.getStock() > 0:
                query.edit_message_text(f"La sorte de café _{name}_ ☕ n'a *PAS* été supprimée "
                                        f"car il reste {cafe.getStock()} cafés ☕ en stock \\!",
                                        parse_mode=ParseMode.MARKDOWN_V2)
                return
            Cafe.removeBrand(chat_id, brand_id)
            logger.debug(f"café removed: id: {cafe.getID()}, name: {name}, stock: {cafe.getStock()}")
            query.edit_message_text(f"La sorte de café _{name}_ ☕ a été supprimée \\!",
                                    parse_mode=ParseMode.MARKDOWN_V2)
        else:
            logger.error("Callback data not recognized: {}".format(data))

    ### Help methods ###

    @staticmethod
    def start(update, context):
        # Do nothing if the command is from an edited message
        if update.edited_message is not None:
            return

        # Update user infos
        user_id = update.message.from_user.id
        chat_id = update.message.chat_id
        User.createOrUpdate(user_id, chat_id, Bot.bot)

        update.message.reply_text(
            "Je suis une machine à café ☕\nTapez /help pour obtenir la liste des commandes."
        )

    @staticmethod
    def help(update, context):
        # Do nothing if the command is from an edited message
        if update.edited_message is not None:
            return
        # Pour BotFather :
        """
cafe - Commande un café
stock - Affiche l'état du stock de cafés
balance - Affiche l'état des comptes
topcafe - Affiche les meilleures sortes de cafés
topdrinker - Affiche les plus grand buveurs
payoff - Supprime les dettes d'un utilisateur
addstock - Ajoute du stock à une sorte de café
addbrand - Ajoute une sorte de café
removebrand - Supprime une sorte de café
setprice - Défini le prix d'un café
help - Affiche l'aide complète
        """
        text = "Voici la liste des commandes à disposition:\n"
        text += "/cafe - Commande un café\n"
        text += "/stock - Affiche l'état des stocks de café\n"
        text += "/balance - Affiche l'état des comptes\n"
        text += "/topcafe - Affiche les meilleures sortes de cafés\n"
        text += "/topdrinker - Affiche les plus grand buveurs"
        # text += "/topcafe [week|month|all] - Affiche les meilleures sortes de cafés\n"
        # text += "/topdrinker [week|month|all] - Affiche les plus grand buveurs"

        member = Utils.getChatMemberFromID(update.message.from_user.id, update.message.chat_id, Bot.bot)
        # Si le membre est créateur du groupe ou si c'est un développeur qui a activé le mode debug
        if member.status == constants.CHATMEMBER_CREATOR or \
                (logger.level == logging.DEBUG and update.message.from_user.id in config.developers):
            text += "\n\nVoici la liste des commandes pour les administrateurs:\n"
            text += "/payoff - Supprime les dettes d'un utilisateur\n"
            text += "/addstock [nb] - Ajoute du stock à une sorte de café\n"
            text += "/addbrand {name} - Ajoute une sorte de café\n"
            text += "/removebrand - Supprime une sorte de café\n"
            text += "/setprice {price} - Défini le prix d'un café\n"

        update.message.reply_text(text)

    """
    @staticmethod
    def getInfos(update, context):
        text = "Voici les informations sur le bot :\n"
        text += "Chat ID : {}\n".format(update.message.chat_id)
        text += "User ID : {}\n".format(update.message.from_user.id)
        text += "Nom : {}\n".format(update.message.chat.title)
        text += "Type : {}\n".format(update.message.chat.type)
        text += "Nombre de membres : {}\n".format(update.message.chat.get_member_count())
        text += "Nombre de groupes : {}\n".format(len(update.message.chat.get_administrators()))

        update.message.reply_text(text)
    """

    # Logging methods

    # /debug : (dés)active le mode debug
    @staticmethod
    def debug(update, context):
        # Do nothing if the command is from an edited message or from a developer
        if update.edited_message is not None \
                or update.message.from_user.id not in config.developers:
            return

        if logger.level == logging.DEBUG:
            logger.setLevel(logging.INFO)
            update.message.reply_text("Mode debug désactivé")
        else:
            logger.setLevel(logging.DEBUG)
            update.message.reply_text("Mode debug activé")

    @staticmethod
    def error(update, context):
        if context is None:
            return
        if update is None:
            logger.error(f'Caught an error with no update : {context.error}')
            return
        """Log Errors caused by Updates."""
        logger.error(f'The update : "{update.to_json()}" caused the following error : "{context.error}"')
        # TODO: set msg to developers ?

    # Utilitary methods

    @staticmethod
    def printErrNotAdmin(update):
        update.message.reply_text("Accès réservé au propriétaire du groupe.")


def main():
    # Create the Updater and pass it your bot's token.
    updater = Updater(config.token, use_context=True)
    Bot.bot = updater.bot
    dispatcher = updater.dispatcher

    #
    # Link commands to functions
    #

    # Base & Developper commands
    dispatcher.add_handler(CommandHandler('start', Bot.start))
    updater.dispatcher.add_handler(CommandHandler('help', Bot.help))
    dispatcher.add_handler(CommandHandler('debug', Bot.debug))
    dispatcher.add_error_handler(Bot.error)

    # Bot custom commands
    dispatcher.add_handler(CommandHandler('cafe', Bot.cafe))
    dispatcher.add_handler(CommandHandler('topcafe', Bot.topcafe))
    dispatcher.add_handler(CommandHandler('balance', Bot.balance))
    dispatcher.add_handler(CommandHandler('stock', Bot.stock))
    dispatcher.add_handler(CommandHandler('addstock', Bot.addStock))
    dispatcher.add_handler(CommandHandler('payoff', Bot.payoff))
    dispatcher.add_handler(CommandHandler('topdrinker', Bot.topdrinker))
    dispatcher.add_handler(CommandHandler('addbrand', Bot.addCoffeeBrand))
    dispatcher.add_handler(CommandHandler('removebrand', Bot.removeCoffeeBrand))
    dispatcher.add_handler(CommandHandler('setprice', Bot.setPrice))

    # dispatcher.add_handler(CommandHandler('infos', Bot.getInfos))

    # Callbacks
    dispatcher.add_handler(CallbackQueryHandler(Bot.callbackHandler))

    # Start the Bot
    updater.start_polling()

    # Run the bot until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT
    updater.idle()


if __name__ == '__main__':
    main()
