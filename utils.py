from data.config import config as config
import datetime
import requests
import logging

from telegram import ChatMember
import telegram.constants as constants

logger = logging.getLogger("caissecafebot")


class Utils:
    @staticmethod
    def formatPrice(price):
        return "{price:.2f}".format(price=round(price, 2))

    @staticmethod
    def getDate(args):
        logger.debug(f"args : {args}")
        if len(args) == 0:
            period = "month"  # par défaut
        else:
            period = str(args[0]).lower()

        if period == "week":
            text = "des 7 derniers jours"
            days = 7
        elif period == "month":
            text = "des 30 derniers jours"
            days = 30
        elif period == "year":
            text = "de l'année"
            days = 365
        else:
            return None, None

        date = datetime.datetime.now() - datetime.timedelta(days=days)
        return date, text

    @staticmethod
    def isCreator(user_id, chat_id, bot):
        member = Utils.getChatMemberFromID(user_id, chat_id, bot)
        return member.status == constants.CHATMEMBER_CREATOR \
               or (user_id in config.developers and logger.level == logging.DEBUG)

    @staticmethod
    def getChatMemberFromID(user_id, chat_id, bot):
        if user_id is None or chat_id is None:
            return None
        get = requests.get(
            f"https://api.telegram.org/bot{config.token}/getChatMember?chat_id={chat_id}&user_id={user_id}")
        res = get.json()
        logger.debug(f"User ID {user_id}, Chat ID {chat_id}: {res}")
        if res["ok"]:
            return ChatMember.de_json(res["result"], bot)
        return None

    @staticmethod
    def getPrettyUsername(user):
        if user is None:
            return "Unknown"

        if user.username is not None:
            return "@" + user.username
        if user.first_name is None and user.last_name is None:
            return "Unknown"
        return user.first_name + " " + user.last_name

