import sqlite3
from datetime import datetime


class DBHelper:
    def __init__(self, dbname="./data/db/caissecafe_db.sqlite"):
        self.dbname = dbname
        self.conn = sqlite3.connect(dbname, check_same_thread=False)
        self.conn.execute("PRAGMA foreign_keys = 1")
        self.setup()

    def setup(self):
        # TODO: Change max TEXT size to 50.
        brand = """CREATE TABLE IF NOT EXISTS brand
            (
                id INTEGER PRIMARY KEY,
                group_id BIGINT NOT NULL,
                name TEXT NOT NULL,
                stock INTEGER DEFAULT 0 CHECK(stock >= 0),
                deleted BOOLEAN DEFAULT 0,
                UNIQUE(group_id, name)
            );"""

        # TODO: Datetime without ms
        cafe = """CREATE TABLE IF NOT EXISTS cafe
            (
                id INTEGER PRIMARY KEY,
                user_id BIGINT NOT NULL,
                group_id BIGINT NOT NULL,
                date DATETIME NOT NULL,
                paid BOOLEAN DEFAULT 0,
                brand_fk INTEGER NOT NULL,
                FOREIGN KEY (brand_fk) REFERENCES brand(id) ON DELETE RESTRICT
            );"""

        price = """CREATE TABLE IF NOT EXISTS price
            (
                group_id BIGINT PRIMARY KEY,
                price DECIMAL(6, 2) DEFAULT 1.0 CHECK(price >= 0)
            );"""

        user = """CREATE TABLE IF NOT EXISTS user
            (
                id BIGINT PRIMARY KEY,
                username TEXT,
                first_name TEXT,
                last_name TEXT
            );"""

        self.conn.execute(brand)
        self.conn.execute(cafe)
        self.conn.execute(price)
        self.conn.execute(user)
        self.conn.commit()

    #
    # INSERTs
    #

    def addCafe(self, user_id, group_id, brand_id):
        stmt = "INSERT INTO cafe (user_id, group_id, date, brand_fk) VALUES (?,?,?,?);"
        args = (user_id, group_id, datetime.now(), brand_id)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def addBrand(self, group_id, name):
        stmt = "INSERT INTO brand (group_id, name) VALUES (?,?);"
        args = (group_id, name)
        new_id = self.conn.execute(stmt, args).lastrowid
        self.conn.commit()
        return new_id

    def genPrice(self, group_id):
        stmt = "INSERT INTO price (group_id) VALUES (?);"
        args = (group_id, )
        self.conn.execute(stmt, args)
        self.conn.commit()

    def addUser(self, user_id, username, first_name, last_name):
        stmt = "INSERT INTO user (id, username, first_name, last_name) VALUES (?,?,?,?);"
        args = (user_id, username, first_name, last_name)
        self.conn.execute(stmt, args)
        self.conn.commit()

    #
    # UPDATEs
    #

    def payoff(self, user_id, group_id):
        stmt = "UPDATE cafe SET paid = 1 WHERE user_id = (?) AND group_id = (?);"
        args = (user_id, group_id)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def incStock(self, brand_id, group_id, quantity):
        stmt = "UPDATE brand SET stock = stock + (?) WHERE brand.id = (?) AND group_id = (?);"
        args = (quantity, brand_id, group_id)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def setDeletedBrand(self, group_id, brand_id, deleted):
        stmt = "UPDATE brand SET deleted = (?) WHERE group_id = (?) AND id = (?);"
        args = (deleted, group_id, brand_id)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def setPrice(self, group_id, price):
        stmt = "UPDATE price SET price = (?) WHERE group_id = (?);"
        args = (price, group_id)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def updateUser(self, user_id, username, first_name, last_name):
        stmt = "UPDATE user SET username = (?), first_name = (?), last_name = (?) WHERE id = (?);"
        args = (username, first_name, last_name, user_id)
        self.conn.execute(stmt, args)
        self.conn.commit()

    #    
    # SELECTs
    #

    def getPrice(self, group_id):
        stmt = "SELECT price FROM price WHERE group_id = (?);"
        args = (group_id,)
        return self.conn.execute(stmt, args).fetchone()

    def getAllBrands(self, group_id):
        stmt = "SELECT * FROM brand WHERE group_id = (?) AND deleted = 0;"
        return self.conn.execute(stmt, (group_id,)).fetchall()

    def getBrandFromID(self, brand_id):
        stmt = "SELECT * FROM brand WHERE id = (?);"
        return self.conn.execute(stmt, (brand_id,)).fetchone()

    def getBrandFromName(self, group_id, name):
        stmt = "SELECT * FROM brand WHERE group_id = (?) AND name = (?);"
        return self.conn.execute(stmt, (group_id, name)).fetchall()

    def getTopDrinker(self, group_id, date_time):
        stmt = """SELECT user_id, COUNT(*) AS cnt FROM cafe INNER JOIN brand ON brand_fk = brand.id
            WHERE cafe.group_id = (?) AND date >= (?) GROUP BY user_id ORDER BY cnt DESC LIMIT 10;"""
        args = (group_id, date_time)
        return self.conn.execute(stmt, args).fetchall()

    def getTopBrands(self, group_id, date_time):
        stmt = """SELECT brand.name, COUNT(*) AS cnt FROM cafe INNER JOIN brand ON brand_fk = brand.id
            WHERE cafe.group_id = (?) AND date >= (?) GROUP BY brand_fk ORDER BY cnt DESC LIMIT 10;"""
        args = (group_id, date_time)
        return self.conn.execute(stmt, args).fetchall()

    def getUserBalance(self, user_id, group_id):
        stmt = "SELECT COUNT(*) FROM cafe WHERE user_id = (?) AND group_id = (?) AND paid = 0;"
        args = (user_id, group_id)
        return self.conn.execute(stmt, args).fetchone()

    def getBalance(self, group_id):
        stmt = "SELECT user_id, COUNT(*) FROM cafe WHERE group_id = (?) AND paid = 0 GROUP BY user_id;"
        args = (group_id,)
        return self.conn.execute(stmt, args).fetchall()

    def getStock(self, group_id):
        stmt = "SELECT id, name, stock FROM brand WHERE group_id = (?) AND deleted = 0;"
        args = (group_id,)
        return self.conn.execute(stmt, args).fetchall()

    def getUserStats(self, group_id, user_id):
        stmt = """SELECT brand.name, COUNT(brand_fk) AS count FROM cafe
            INNER JOIN brand ON brand_fk = brand.id
            WHERE cafe.group_id = (?) AND user_id = (?) AND deleted = 0
            GROUP BY brand.id;"""
        args = (group_id, user_id)
        return self.conn.execute(stmt, args).fetchall()

    def getUser(self, user_id):
        stmt = "SELECT * FROM user WHERE id = (?);"
        args = (user_id, )
        return self.conn.execute(stmt, args).fetchone()


#
# Accessed by other files
#
DB = DBHelper()
