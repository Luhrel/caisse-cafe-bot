Caisse Café Bot
===============

[@CaisseCafeBot](https://t.me/CaisseCafeBot)'s official repository.

# Description

## Bot's commands

This bot allows you to:
- /cafe: consume a coffee (café)
- /stock: see the stock status
- /topcafe: see the most drunk coffees
- /topdrinker: see the biggest consumers of coffee
- /balance: see how much money users needs to pay to the group owner
    
### Admin commands

- `/payoff`: pays off all the debts of a user
- `/addstock [quantity]`: add a some coffees (of a specific brand) to the stock
- `/addbrand {name}`: add a new brand of coffee
- `/removebrand`: remove a brand
- `/setprice {price}`: set the price of the coffees

# TODO

- Make a viable pad:
  - C/R: Reset dial or cancel operation if empty
```
[7][8][9]
[4][5][6]
[1][2][3]
[C/R][0][OK]
```

- Remove all command arguments:
  - `[quantity]` (`/addstock`)
  - `{price}` (`/setprice`)
  - `{name}` (`/addbrand`)
- Add "Cancel" buttons

# Build commands

Build & run for testing:
```
docker build -t caissecafe . && docker compose up
```
Publish for ARM64 in the container repository:
```
docker buildx build --platform linux/arm64 -t registry.gitlab.com/luhrel/caisse-cafe-bot:latest --push .
```

## Troubleshooting
### Exec format error
```
docker run --rm --privileged tonistiigi/binfmt --install all
```
